extends Node


export var next_scene : PackedScene

func _input(event):
	if event is InputEventKey and event.is_pressed():
		var event_key := event as InputEventKey
		match event_key.scancode:
			KEY_H:
				Network.server()
				get_tree().change_scene_to(next_scene)
			KEY_J:
				Network.client()
				get_tree().change_scene_to(next_scene)
