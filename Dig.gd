extends RayCast

signal dig
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var volume : Volume = get_node("/root/Main/Thing/Volume")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if get_tree().get_frame() % 6 == 0 and Input.is_action_pressed("dig") or Input.is_action_just_pressed("dig"):
		#cast_to = $"../../Camera".project_local_ray_normal(get_tree().root.get_mouse_position()) * 32
		force_raycast_update()
		if is_colliding():
			print(get_collision_point())
			volume.dig(get_collision_point(), rand_range(1.0, 1.5))
			emit_signal("dig")
