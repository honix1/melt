extends Node


const SERVER_IP = "127.0.0.1"
const SERVER_PORT = 8088
const MAX_PLAYERS = 8

func server():
	var peer := NetworkedMultiplayerENet.new()
	var err := peer.create_server(SERVER_PORT, MAX_PLAYERS)
	if err != OK:
		print("create_server error %d" % err)
	get_tree().network_peer = peer

func client():
	var peer := NetworkedMultiplayerENet.new()
	var err := peer.create_client(SERVER_IP, SERVER_PORT)
	if err != OK:
		print("create_client error %d" % err)
	get_tree().network_peer = peer
