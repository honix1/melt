extends MultiMeshInstance


onready var volume : Volume = get_node("../Volume")

func _ready():
	if visible:
		redraw()

func redraw():
	multimesh.instance_count = volume.side * volume.side * volume.side
	print("Instance count %s" % multimesh.instance_count)

	var i : int = 0

	for x in range(0, volume.side):
		for y in range(0, volume.side):
			for z in range(0, volume.side):
				var f : float = volume.get_byte(x, y, z) / 255.0
				var scale : float = 0.01

				multimesh.set_instance_color(i, Color.from_hsv(0, 0, f))
				multimesh.set_instance_transform(
					i,
					Transform(
						Basis().scaled(Vector3.ONE * scale),
						volume.get_pos(x, y, z))
				)

				if i % 512 == 0:
					yield(get_tree(), "idle_frame")

				i += 1
