# Typical lobby implementation; imagine this being in /root/lobby.

extends Node

export var playerScene : PackedScene
export var startPos : NodePath

# Connect all functions
func _ready():
	var my_id := get_tree().get_network_unique_id()
	var player := playerScene.instance() as Player
	player.name = str(my_id)
	player.set_network_master(my_id)
	add_child(player)

	player.global_transform.origin = get_node(startPos).global_transform.origin

	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")

# Player info, associate ID to data
var player_info = {}
# Info we send to other players
var my_info = { name = "Johnson Magenta", favorite_color = Color8(255, 0, 255) }

func _player_connected(id):
	# Called on both clients and server when a peer connects. Send my info to it.
	rpc_id(id, "register_player", my_info)

	var player := playerScene.instance()
	player.name = str(id)
	player.set_network_master(id)
	add_child(player)

	player.global_transform.origin = get_node(startPos).global_transform.origin

func _player_disconnected(id):
	player_info.erase(id) # Erase player from info.

func _connected_ok():
	pass # Only called on clients, not server. Will go unused; not useful here.

func _server_disconnected():
	pass # Server kicked us; show error and abort.

func _connected_fail():
	pass # Could not even connect to server; abort.

remote func register_player(info):
	# Get the id of the RPC sender.
	var id = get_tree().get_rpc_sender_id()
	# Store the info
	player_info[id] = info

	# Call function to update lobby UI here
