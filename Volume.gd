class_name Volume
extends Spatial


signal volume_update(pos, aabb)

const side : int = 64
const width : float = 8.0

var noise : OpenSimplexNoise
var noise_hi : OpenSimplexNoise
var array : PoolByteArray

func _ready():
	array = PoolByteArray([])
	array.resize(side * side * side)

	noise = OpenSimplexNoise.new()

	#noise.seed = randi()
	noise.octaves = 2
	noise.period = 20.0
	noise.persistence = 0.9

	noise_hi = OpenSimplexNoise.new()

	noise_hi.seed = randi()
	noise_hi.octaves = 4
	noise_hi.period = 10.0
	noise_hi.persistence = 0.5

	#fill_sphere()
	fill_ground()

func fill_ground():

	#var plane : Plane = Plane(Vector3.UP, 1.0)
	for x in range(0, side):
		for y in range(0, side):
			for z in range(0, side):
				var nv : Vector3 = Vector3(x, y, z) / side
				var value : float = 0
				value = nv.y - 0.4
				value += noise.get_noise_3d(x, y, z) * 0.05
				#value = clamp(value, 0, 1)
				#value = max(value, 1-sphere(nv, center + Vector3.UP * 0.2, 0.5))
				set_byte(int(value * 255), x, y, z)

func sphere(probe : Vector3, pos : Vector3, radius : float) -> float:
	return probe.distance_to(pos) / radius

func fill_sphere():
	var center : Vector3 = Vector3.ONE * 0.5
	for x in range(0, side):
		for y in range(0, side):
			for z in range(0, side):
				var nv : Vector3 = Vector3(x, y, z) / side
				var value : float = 0
				value = sphere(nv, center, 0.9)
				#value = max(value, 1-sphere(nv, center + Vector3.UP * 0.2, 0.5))
				set_byte(int(value * 255), x, y, z)

func fill_gradient():
	for x in range(0, side):
		for y in range(0, side):
			for z in range(0, side):
				set_byte(int(float(y) / side * 255), x, y, z)

func get_byte(x : int, y : int, z : int) -> int:
	return array[z * side * side + y * side + x]

func set_byte(value : int, x : int, y : int, z : int):
	value = clamp(value, 0, 255)
	array[z * side * side + y * side + x] = value

func get_pos(x : int, y : int, z : int) -> Vector3:
	return Vector3(x, y, z) * width / side

func vclamp(v : Vector3, from : Vector3, to : Vector3) -> Vector3:
	return Vector3(
		clamp(v.x, from.x, to.x),
		clamp(v.y, from.y, to.y),
		clamp(v.z, from.z, to.z)
	)

remote func dig(pos : Vector3, radius : float):
	rpc("dig_internal", pos, radius)

remotesync func dig_internal(pos : Vector3, radius : float):
	var ipos : Vector3 = pos / width * side
	var iradius : float = radius / width * side

	var vmin : Vector3 = ipos - Vector3.ONE * iradius
	var vmax : Vector3 = ipos + Vector3.ONE * iradius

	vmin = vclamp(vmin, Vector3.ZERO, Vector3.ONE * side)
	vmax = vclamp(vmax, Vector3.ZERO, Vector3.ONE * side)

	for x in range(vmin.x, vmax.x):
		for y in range(vmin.y, vmax.y):
			for z in range(vmin.z, vmax.z):
				var nv : Vector3 = Vector3(x, y, z) / side
				var value : float = 0
				value = get_byte(x, y, z) / 255.0
				value = max(value, 1-sphere(nv, ipos / side, iradius / side) + noise_hi.get_noise_3d(x, y, z) * 0.25)
				set_byte(int(value * 255), x, y, z)

	emit_signal("volume_update", pos, AABB(pos, Vector3.ONE * radius * 2.0))
