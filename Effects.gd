extends Spatial


export var digParticles : PackedScene

func _on_Volume_volume_update(pos, aabb):
	var instance := digParticles.instance() as CPUParticles
	add_child(instance)
	instance.global_transform.origin = pos
	instance.emitting = true

	yield(get_tree().create_timer(1.5), "timeout")

	instance.queue_free()
