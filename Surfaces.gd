extends Spatial


signal generated

export var material : Material
onready var volume : Volume = get_node("../Volume")

func _ready():
	var surfaces := []

	var s : int = volume.side / 8
	for x in range(0, volume.side, s):
		for y in range(0, volume.side, s):
			for z in range(0, volume.side, s):
				var v := Vector3(x, y, z)
				var overlap := Vector3(
					1 if x < volume.side-s else 0,
					1 if y < volume.side-s else 0,
					1 if z < volume.side-s else 0
				)
				var surface := Surface.new(
					v,
					v + Vector3.ONE * s + overlap,
					volume,
					material
				)
				add_child(surface)
				surfaces.append(surface)

	for step in [4, 1]:
		var i : int = 0
		for surface in surfaces:
			if i % 12 == 0:
				yield(get_tree(), "idle_frame")
			surface.march(step)
			i += 1

	emit_signal("generated")
