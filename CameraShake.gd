extends Tween


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_RayCast_dig():
	reset_all()
#	interpolate_property(get_parent(), "rotation_degrees",
#		Vector3(0,-180,0), Vector3(-3,-180,0), 0.3,
#		Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
#	start()
#	yield(self, "tween_completed")
	interpolate_property(get_parent(), "rotation_degrees",
		Vector3(-3,-180,0), Vector3(0,-180,0), 0.6,
		Tween.TRANS_BACK, Tween.EASE_IN_OUT)
	start()
